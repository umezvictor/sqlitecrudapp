package com.example.sqlitecrudapp;

import android.content.Intent;
import android.os.Bundle;

import com.example.sqlitecrudapp.data.DatabaseHandler;
import com.example.sqlitecrudapp.model.Item;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Handler;
import android.util.Log;
import android.view.View;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;

import java.util.List;

public class MainActivity extends AppCompatActivity {
//create fields
    //these fields will handle the alerts

    private AlertDialog.Builder builder;
    private AlertDialog dialog;
    private Button saveButton;

    //input fields
    private EditText babyItem;
    private EditText itemQuantity;
    private EditText itemColor;
    private EditText itemSize;

    //database handler class
    private DatabaseHandler dbHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);//ensures backward compatibility

        dbHandler = new DatabaseHandler(this);

        //bypass main activity if user has already added items before
        byPassActivity();

        //check if item was saved
        List<Item> items = dbHandler.getAllItems();//returns a list of items
        for(Item item : items){
            Log.d("Main ", "oncreate " + item.getItemName());
        }

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //when the fab is clicked, the popup should display
                createPopupDialog();
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        //.setAction("Action", null).show();
            }
        });
    }

    private void byPassActivity() {
        //if item count is > 0, move to list activity
        if(dbHandler.getItemsCount() > 0){
            startActivity( new Intent(MainActivity.this, ListActivity.class));
            //get rid of any previous activity
            finish();
        }
    }

    //pass in View as parameter so we can grab the view where the save
    //item is been shown
    private void saveItem(View view) {
        //save each item
        Item item = new Item();
        //get input from ui
        String itemName = babyItem.getText().toString().trim();
        String color = itemColor.getText().toString().trim();
        int quantity = Integer.parseInt(itemQuantity.getText().toString().trim());
        int size = Integer.parseInt(itemSize.getText().toString().trim());

        //dateadded will be added automatically by the db handler class

        item.setItemName(itemName);
        item.setItemColor(color);
        item.setItemSize(size);
        item.setItemQuantity(quantity);

        //save to db
        dbHandler.addItem(item);
        Snackbar.make(view, "item saved", Snackbar.LENGTH_SHORT)
                .show();


        //wait fror a second, then dismiss the popup
        // while dismissing, we want to start a new activity


        //use this to wait for one second before dismissing the popup
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();//dismiss popup
                //move to details screen
                startActivity(new Intent(MainActivity.this, ListActivity.class));
            }
        }, 1200); //interval - slightly above 1second

    }

    private void createPopupDialog() {

        //init dialog builder
        builder = new AlertDialog.Builder(this);
        //inflate the popup.xml file here
        //i.e it should be displayed when this method is called
        View view = getLayoutInflater().inflate(R.layout.popup,null);
        //get input fields in the popup form and set up view
        babyItem = view.findViewById(R.id.babyItem);
        itemColor = view.findViewById(R.id.itemColor);
        itemQuantity = view.findViewById(R.id.itemQuantity);
        itemSize = view.findViewById(R.id.itemSize);

        //I am calling the save button here because it's not related to mainactviity
        saveButton = view.findViewById(R.id.save_btn);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //save item
                //we passed this view, because snackbar needs to be attached to a view
                if(!babyItem.getText().toString().isEmpty()
                        && !itemColor.getText().toString().isEmpty()
                        && !itemQuantity.getText().toString().isEmpty()
                        && !itemSize.getText().toString().isEmpty()){
                    saveItem(view);
                }else{
                    Snackbar.make(view, "Empty fields not allowed", Snackbar.LENGTH_SHORT)
                            .show();
                }

            }
        });

        //invoke builder
        builder.setView(view);
        //create the dialog object
        dialog = builder.create();
        dialog.show();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}