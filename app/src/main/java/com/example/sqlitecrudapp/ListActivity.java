package com.example.sqlitecrudapp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sqlitecrudapp.data.DatabaseHandler;
import com.example.sqlitecrudapp.model.Item;
import com.example.sqlitecrudapp.ui.RecyclerViewAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

public class ListActivity extends AppCompatActivity {

    public static final String TAG = "ListActivity";

    //bring in the recyclerViewAdapter class
    private RecyclerView recyclerView;
    private RecyclerViewAdapter recyclerViewAdapter;
    private List<Item> itemList;
    private DatabaseHandler dbHandler;


    //floating action button
    private FloatingActionButton fab;

    private AlertDialog.Builder builder;
    private AlertDialog alertDialog;

    //input fields
    private EditText babyItem;
    private EditText itemQuantity;
    private EditText itemColor;
    private EditText itemSize;

    private Button saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        //instantiate recyclerview
        recyclerView = findViewById(R.id.items_list_recyclerview);

        //use fab to launch activity to add new item
        fab = findViewById(R.id.fab);

        dbHandler = new DatabaseHandler(this);
        recyclerView.setHasFixedSize(true);//makes sure things are alignedd properly
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        itemList = new ArrayList<>();

        //get items from db
        itemList = dbHandler.getAllItems();

        for(Item item : itemList){
            Log.d(TAG, "oncreate " + item.getItemName());
        }

        //show items in recyclerview
        recyclerViewAdapter = new RecyclerViewAdapter(this, itemList);
        recyclerView.setAdapter(recyclerViewAdapter);
        //update recyclerview when there's new data or data removed or edited
        recyclerViewAdapter.notifyDataSetChanged();

        fab.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                //create popup dialog
                createPopupDialog();
            }
        });
    }

    //ensure you bring in dialog builder
    private void createPopupDialog() {
        builder = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.popup, null);

        //get input fields in the popup form and set up view
        babyItem = view.findViewById(R.id.babyItem);
        itemColor = view.findViewById(R.id.itemColor);
        itemQuantity = view.findViewById(R.id.itemQuantity);
        itemSize = view.findViewById(R.id.itemSize);

        //I am calling the save button here because it's not related to mainactviity
        saveButton = view.findViewById(R.id.save_btn);


        builder.setView(view);
        alertDialog = builder.create();
        alertDialog.show();

        saveButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                //we passed this view, because snackbar needs to be attached to a view
                if(!babyItem.getText().toString().isEmpty()
                        && !itemColor.getText().toString().isEmpty()
                        && !itemQuantity.getText().toString().isEmpty()
                        && !itemSize.getText().toString().isEmpty()){
                    saveItem(view);
                }else{
                    Snackbar.make(view, "Empty fields not allowed", Snackbar.LENGTH_SHORT)
                            .show();
                }
            }
        });
    }

    //save item
    private void saveItem(View view) {
        //save each item
        Item item = new Item();
        //get input from ui
        String itemName = babyItem.getText().toString().trim();
        String color = itemColor.getText().toString().trim();
        int quantity = Integer.parseInt(itemQuantity.getText().toString().trim());
        int size = Integer.parseInt(itemSize.getText().toString().trim());

        //dateadded will be added automatically by the db handler class

        item.setItemName(itemName);
        item.setItemColor(color);
        item.setItemSize(size);
        item.setItemQuantity(quantity);

        //save to db
        dbHandler.addItem(item);
        Snackbar.make(view, "item saved", Snackbar.LENGTH_SHORT)
                .show();


        //wait for a second, then dismiss the popup
        // while dismissing, we want to start a new activity


        //use this to wait for one second before dismissing the popup
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                alertDialog.dismiss();//dismiss popup
                //this case we remain within list activity
                startActivity(new Intent(ListActivity.this, ListActivity.class));
                //kill the previous activity
                finish();
            }
        }, 1200); //interval - slightly above 1second

    }
}