package com.example.sqlitecrudapp.util;

public class Constants {
    public static final int DB_VERSION = 1;
    public static final String DB_NAME = "babyList";
    public static final String TABLE_NAME = "baby_tbl";

    //table columns
    public static final String KEY_ID = "id";
    public static final String KEY_BABY_ITEM = "baby_item";
    public static final String KEY_ITEM_QTY = "quantity";
    public static final String KEY_ITEM_COLOR = "color";
    public static final String KEY_ITEM_SIZE = "size";
    public static final String KEY_DATE_ADDED = "date_added";

}
