package com.example.sqlitecrudapp.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sqlitecrudapp.R;
import com.example.sqlitecrudapp.data.DatabaseHandler;
import com.example.sqlitecrudapp.model.Item;
import com.google.android.material.snackbar.Snackbar;

import java.text.MessageFormat;
import java.util.List;

//this recyclerviewAdapter class will be called in the listactivity class

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private Context context;
    private List<Item> itemList;


    private AlertDialog.Builder builder;
    private AlertDialog dialog;
    private LayoutInflater inflater;

    //create constructor
    public RecyclerViewAdapter(Context context, List<Item> itemList){
        this.context = context;
        this.itemList = itemList;
    }

    //this will get the rows
    @NonNull
    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.items_list_row, viewGroup, false);

        //return Viewholder
        return new ViewHolder(view, context);
    }

    //this is where we bind the view and data
    //appended descriptive static text beside each field for clarity
    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapter.ViewHolder viewHolder, int position) {
        Item item = itemList.get(position);
        //bind data to the viewholder
        viewHolder.itemName.setText(MessageFormat.format("Item: {0}", item.getItemName()));
        viewHolder.itemColor.setText(MessageFormat.format("Color: {0}", item.getItemColor()));
        viewHolder.size.setText(MessageFormat.format("Size{0}: ", item.getItemSize()));
        viewHolder.quantity.setText(MessageFormat.format("Qty: {0}", item.getItemQuantity()));
        viewHolder.dateAdded.setText(MessageFormat.format("Added on: {0}", item.getDateCreated()));
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    //this class will be privete by default, i made it  to clear errors
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        //create instance variables partaiing to items-list-rows
        public TextView itemName;
        public TextView itemColor;
        public TextView quantity;
        public TextView size;
        public TextView dateAdded;
        public Button editBtn;
        public Button deleteBtn;

        public int id;

        //pass in context as well
        public ViewHolder(@NonNull View itemView, Context ctx) {
            super(itemView);
            context = ctx;

            itemName = itemView.findViewById(R.id.item_name);
            itemColor = itemView.findViewById(R.id.item_color);
            quantity = itemView.findViewById(R.id.item_quantity);
            size = itemView.findViewById(R.id.item_size);

            dateAdded = itemView.findViewById(R.id.item_date);
            editBtn = itemView.findViewById(R.id.edit_btn);
            deleteBtn = itemView.findViewById(R.id.delete_btn);

            editBtn.setOnClickListener(this);
            deleteBtn.setOnClickListener(this);

        }

        //when the delete or edit button is clicked

        @Override
        public void onClick(View view) {
            //get current position in the adapter
            //i.e the current item selected in the recyclerview

            int position;
            //reassigned position just incase something changed
            position = getAdapterPosition();
            //get the item object
            Item item = itemList.get(position);

            switch (view.getId()){
                case R.id.edit_btn:
                    //edit item

                    Log.d("EDIT ", "EDIT BTN CLICKED");
                    editItem(item);
                    break;
                case R.id.delete_btn:
                    //delete item
                    //reassigned position just incase something changed

                    deleteItem(item.getId());
                    break;
            }
        }

        private void deleteItem(final int id){

            builder = new AlertDialog.Builder(context);
            inflater = LayoutInflater.from(context);
            View view = inflater.inflate(R.layout.confirmation_popup, null);

            Button noButton = view.findViewById(R.id.conf_no_button);
            Button yesButton = view.findViewById(R.id.conf_yes_button);

            builder.setView(view);

            builder.setView(view);
            dialog = builder.create();
            dialog.show();

            //when user clicks on yes button to delete
            yesButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DatabaseHandler db = new DatabaseHandler(context);
                    db.deleteItem(id);
                    //after deleting from database, delete the card view containing the item
                    //remove the corresponding item object from the list
                    itemList.remove(getAdapterPosition());
                    //notify the adapter that an item has been removed from the list
                    notifyItemRemoved(getAdapterPosition());

                    dialog.dismiss();
                }
            });

            //when user clicks on "no" button
            noButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });


        }

        private void editItem(final Item newItem) {
            //populate popup with current object data
            //get the current item
            //  final Item item = itemList.get(getAdapterPosition());

            builder = new AlertDialog.Builder(context);
            inflater = LayoutInflater.from(context);
            final View view = inflater.inflate(R.layout.popup, null);

            Button saveButton;

            //input fields
            final EditText babyItem;
            final EditText itemQuantity;
            final EditText itemColor;
            final EditText itemSize;

            TextView title;

            //get input fields in the popup form and set up view
            babyItem = view.findViewById(R.id.babyItem);
            itemColor = view.findViewById(R.id.itemColor);
            itemQuantity = view.findViewById(R.id.itemQuantity);
            itemSize = view.findViewById(R.id.itemSize);

            //I am calling the save button here because it's not related to main actviity
            saveButton = view.findViewById(R.id.save_btn);
            saveButton.setText(R.string.Update);
            title = view.findViewById(R.id.title);

            title.setText(R.string.edit_item);

            babyItem.setText(newItem.getItemName());
            itemQuantity.setText(String.valueOf(newItem.getItemQuantity()));
            itemColor.setText(newItem.getItemColor());
            itemSize.setText(String.valueOf(newItem.getItemSize()));

            builder.setView(view);
            dialog = builder.create();
            dialog.show();

            //get the save button
            saveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //update item
                    DatabaseHandler dbHandler = new DatabaseHandler(context);

                    newItem.setItemName(babyItem.getText().toString());
                    newItem.setItemColor(itemColor.getText().toString());
                    newItem.setItemQuantity(Integer.parseInt(itemQuantity.getText().toString()));
                    newItem.setItemSize(Integer.parseInt(itemSize.getText().toString()));

                    if(!babyItem.getText().toString().isEmpty()
                            && !itemColor.getText().toString().isEmpty()
                            && !itemQuantity.getText().toString().isEmpty()
                            && !itemSize.getText().toString().isEmpty()){

                        dbHandler.updateItem(newItem);
                        notifyItemChanged(getAdapterPosition(), newItem);//important
                    }else{
                        Snackbar.make(view, "Fields empty",
                                Snackbar.LENGTH_SHORT )
                                .show();
                    }

                    dialog.dismiss();

                }
            });

        }
    }

}
