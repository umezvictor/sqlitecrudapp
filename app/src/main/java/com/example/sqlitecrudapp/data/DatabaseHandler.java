package com.example.sqlitecrudapp.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

import com.example.sqlitecrudapp.model.Item;
import com.example.sqlitecrudapp.util.Constants;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {

    //init context
    private final Context context;

    public DatabaseHandler(@Nullable Context context) {
        super(context, Constants.DB_NAME, null, Constants.DB_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //create table here
        String CREATE_BABY_TABLE = "CREATE TABLE " + Constants.TABLE_NAME + "("
                + Constants.KEY_ID + " INTEGER PRIMARY KEY,"
                + Constants.KEY_BABY_ITEM + " INTEGER,"
                + Constants.KEY_ITEM_COLOR + " TEXT,"
                + Constants.KEY_ITEM_QTY + " INTEGER,"
                + Constants.KEY_ITEM_SIZE + " INTEGER,"
                + Constants.KEY_DATE_ADDED + " LONG);";

        db.execSQL(CREATE_BABY_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        //drop the table if it exists
        db.execSQL("DROP TABLE IF EXISTS " + Constants.TABLE_NAME);
        //create the table
        onCreate(db);
    }

    //crud operations

    //add item
    public void addItem(Item item){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Constants.KEY_BABY_ITEM, item.getItemName());
        values.put(Constants.KEY_ITEM_COLOR, item.getItemColor());
        values.put(Constants.KEY_ITEM_QTY, item.getItemQuantity());
        values.put(Constants.KEY_ITEM_SIZE, item.getItemSize());
        values.put(Constants.KEY_DATE_ADDED, java.lang.System.currentTimeMillis());//timestamp

        //insert row
        db.insert(Constants.TABLE_NAME, null, values);
        Log.d("DBHandler ", "item added");
    }

    //get item
    public Item getItem(int id){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(Constants.TABLE_NAME,
                new String[]{Constants.KEY_ID,
                Constants.KEY_BABY_ITEM,
                Constants.KEY_ITEM_COLOR,
                Constants.KEY_ITEM_QTY,
                Constants.KEY_ITEM_SIZE,
                Constants.KEY_DATE_ADDED},
                Constants.KEY_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        //check if cursor is null, i.e if an item was retrieved
        if(cursor != null)
            cursor.moveToFirst();//move to the next item
        //create Item object
        Item item = new Item();
        if (cursor != null) {
            item.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.KEY_ID))));
            item.setItemName(cursor.getString(cursor.getColumnIndex(Constants.KEY_BABY_ITEM)));
            item.setItemColor(cursor.getString(cursor.getColumnIndex(Constants.KEY_ITEM_COLOR)));
            item.setItemQuantity(cursor.getInt(cursor.getColumnIndex(Constants.KEY_ITEM_QTY)));
            item.setItemSize(cursor.getInt(cursor.getColumnIndex(Constants.KEY_ITEM_SIZE)));
            //item.setItemName(cursor.getString(cursor.getColumnIndex(Constants.KEY_BABY_ITEM)));

            //convert timestamp to something readable
            DateFormat dateFormat = DateFormat.getDateInstance();
            String formatedDate = dateFormat.format(new Date(cursor.getLong(cursor.getColumnIndex(Constants.KEY_DATE_ADDED)))
            .getTime());//will be formatted to something like feb 23 2010

            item.setDateCreated(formatedDate);
        }

        return item;
    }

    //get all items
    public List<Item> getAllItems(){
        SQLiteDatabase db = this.getReadableDatabase();
        List<Item> itemList = new ArrayList<>();

        Cursor cursor = db.query(Constants.TABLE_NAME,
                new String[]{Constants.KEY_ID,
                        Constants.KEY_BABY_ITEM,
                        Constants.KEY_ITEM_COLOR,
                        Constants.KEY_ITEM_QTY,
                        Constants.KEY_ITEM_SIZE,
                        Constants.KEY_DATE_ADDED},
                null, null, null, null,Constants.KEY_DATE_ADDED + " DESC");

        if(cursor.moveToFirst()){
            do{
                Item item = new Item();
                item.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.KEY_ID))));
                item.setItemName(cursor.getString(cursor.getColumnIndex(Constants.KEY_BABY_ITEM)));
                item.setItemColor(cursor.getString(cursor.getColumnIndex(Constants.KEY_ITEM_COLOR)));
                item.setItemQuantity(cursor.getInt(cursor.getColumnIndex(Constants.KEY_ITEM_QTY)));
                item.setItemSize(cursor.getInt(cursor.getColumnIndex(Constants.KEY_ITEM_SIZE)));
                //convert timestamp to something readable
                DateFormat dateFormat = DateFormat.getDateInstance();
                String formatedDate = dateFormat.format(new Date(cursor.getLong(cursor.getColumnIndex(Constants.KEY_DATE_ADDED)))
                        .getTime());//will be formatted to something like feb 23 2010

                item.setDateCreated(formatedDate);

                //add to arraylist
                itemList.add(item);
            }while (cursor.moveToNext());
        }

        return itemList;
    }

    //update item
    public int updateItem (Item item){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Constants.KEY_BABY_ITEM, item.getItemName());
        values.put(Constants.KEY_ITEM_COLOR, item.getItemColor());
        values.put(Constants.KEY_ITEM_QTY, item.getItemQuantity());
        values.put(Constants.KEY_ITEM_SIZE, item.getItemSize());
        values.put(Constants.KEY_DATE_ADDED, java.lang.System.currentTimeMillis());//timestamp

        //update row
        return db.update(Constants.TABLE_NAME, values, Constants.KEY_ID + "=?",
                new String[]{String.valueOf(item.getId())});
    }

    //delete item
    public void deleteItem(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Constants.TABLE_NAME,
                Constants.KEY_ID + "=?",
                new String[]{String.valueOf(id)});
        //close
        db.close();
    }

    //get items count - total number of items
    public int getItemsCount(){
        String countQuery = "SELECT * FROM " + Constants.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        return cursor.getCount();
    }
}
